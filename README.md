# Using useState react.js hook
**To read**: [<link >]

**Estimated reading time**: 3min

## Story Outline
This is the story where a user can see the practical example of using `useState` React.js hook.

## Story Organization
**Story Branch**: master
> `git checkout useState-hook-react`


Tags: #reactjs #hooks #useState